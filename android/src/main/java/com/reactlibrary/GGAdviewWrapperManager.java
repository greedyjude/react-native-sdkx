package com.reactlibrary;

import android.util.Log;

import com.facebook.react.bridge.*;
import com.facebook.react.uimanager.ThemedReactContext;
import com.facebook.react.uimanager.ViewGroupManager;
import com.facebook.react.uimanager.annotations.ReactProp;


public class GGAdviewWrapperManager extends ViewGroupManager<GGAdviewWrapper> {

    @Override
    public String getName() {
        return "GGAdviewWrapper";
    }

    @Override
    public GGAdviewWrapper createViewInstance(ThemedReactContext reactContext) {
        final GGAdviewWrapper wrapperView = new GGAdviewWrapper(reactContext);
        return wrapperView;
    }

    @ReactProp(name = "uniqueId")
    public void setUniqueEventId(GGAdviewWrapper adViewWrapper, String id) {
        adViewWrapper.setUniqueEventId(id);
    }

    @ReactProp(name = "unitId")
    public void setUnitId(GGAdviewWrapper adViewWrapper, String unitId) {
        adViewWrapper.setUnitId(unitId);
    }

    @ReactProp(name = "adsMaxHeight")
    public void setAdsMaxHeight(GGAdviewWrapper adviewWrapper, int maxHeight) {
        Log.d(getName(), "adsMaxHeight " + maxHeight);
        adviewWrapper.setAdsMaxHeight(maxHeight);
    }

    @ReactProp(name = "adsMaxWidth", defaultInt = -1)
    public void setAdsMaxWidth(GGAdviewWrapper adviewWrapper, int maxWidth) {
        Log.d(getName(), "adsMaxWidth " + maxWidth);
        adviewWrapper.setAdsMaxWidth(maxWidth);
    }

}
