package com.reactlibrary;

import java.util.*;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;

import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.MeasureSpec;

import com.facebook.react.bridge.*;
import com.facebook.react.common.MapBuilder;
import com.facebook.react.uimanager.ThemedReactContext;
import com.facebook.react.uimanager.ViewGroupManager;
import com.facebook.react.uimanager.events.RCTEventEmitter;
import com.facebook.react.uimanager.annotations.ReactProp;
import com.facebook.react.uimanager.annotations.ReactPropGroup;

import com.greedygame.core.GreedyGameAds;
import com.greedygame.core.adview.GGAdview;
import com.greedygame.core.adview.interfaces.AdLoadCallback;
import com.greedygame.core.adview.modals.AdRequestErrors;
import com.greedygame.core.adview.modals.RefreshPolicy;
import com.greedygame.core.interfaces.DestoryEventListener;

class GGAdviewManager extends ViewGroupManager<GGAdview> {

    @Override
    public String getName() {
        return "GGAdview";
    }

    @Override
    public GGAdview createViewInstance(ThemedReactContext reactContext) {
        final GGAdview adView = new GGAdview(reactContext);
        GreedyGameAds.addDestroyEventListener(new DestoryEventListener(){
            @Override
            public void onDestroy(){
                Log.d(getName(),"SDK-Destroyed");
                adView.requestLayout();
                adView.post(new Runnable() {
                    @Override
                    public void run() {
                        Log.d(getName(),"ReDrawing the view");
                        adView.measure(MeasureSpec.makeMeasureSpec(adView.getWidth(), MeasureSpec.EXACTLY),
                                MeasureSpec.makeMeasureSpec(adView.getHeight(), MeasureSpec.EXACTLY));
                        adView.layout(adView.getLeft(), adView.getTop(), adView.getRight(), adView.getBottom());
                    }
                });
            }
        });
        return adView;
    }

    @ReactProp(name = "unitId")
    public void setUnitId(GGAdview adview, String unitId) {
        Log.d(getName(), "New Unit Id" + unitId);
        adview.setUnitId(unitId);
        loadAd(adview);
    }

    @ReactProp(name = "adsMaxHeight")
    public void setAdsMaxHeight(GGAdview adview, int adsMaxHeight) {
        adview.setAdsMaxHeight(adsMaxHeight);
    }

    @ReactProp(name = "adsMaxWidth")
    public void setAdsMaxWidth(GGAdview adview, int adsMaxWidth) {
        adview.setAdsMaxWidth(adsMaxWidth);
    }

    @ReactProp(name="refreshPolicy")
    public void setRefreshPolicy(GGAdview adview,String policy){
        if(policy.equalsIgnoreCase("manual")) {
            adview.setRefreshPolicy(RefreshPolicy.MANUAL);
        } else {
            adview.setRefreshPolicy(RefreshPolicy.AUTO);
        }
    }

    @ReactMethod
    void loadAd(final GGAdview adView) {
        Log.d(getName(), "Loading ad for unitId");
        Log.d(getName(),"AdSize before load"+adView.getWidth()+"X"+adView.getHeight());
        final ReactContext reactContext = (ReactContext) adView.getContext();
        adView.loadAd(new AdLoadCallback() {
            @Override
            public void onReadyForRefresh() {
                Utils.emit(reactContext,"ready-for-refresh-"+adView.getUnitId());
                Log.d(getName(), "Ad Ready for refresh");
            }

            @Override
            public void onUiiClosed() {
                Utils.emit(reactContext,"uii-closed-"+adView.getUnitId());
                Log.d(getName(), "Uii closed");
            }

            @Override
            public void onUiiOpened() {
                Utils.emit(reactContext,"uii-opened-"+adView.getUnitId());
                Log.d(getName(), "Uii Opened");
            }

            @Override
            public void onAdLoadFailed(AdRequestErrors cause) {
                Log.d(getName(), "Ad Load Failed " + cause);
                WritableMap map = Arguments.createMap();
                map.putString("cause",cause.toString());
                Utils.emit(reactContext,"ad-load-failed-"+adView.getUnitId(),map);
            }

            @Override
            public void onAdLoaded() {
                Log.d(getName(), "Ad Loaded"+adView.getUnitId());
                adView.requestLayout();
                adView.post(new Runnable() {
                    @Override
                    public void run() {
                        Log.d(getName(),"ReDrawing the view");
                        adView.measure(MeasureSpec.makeMeasureSpec(adView.getWidth(), MeasureSpec.EXACTLY),
                                MeasureSpec.makeMeasureSpec(adView.getHeight(), MeasureSpec.EXACTLY));
                        adView.layout(adView.getLeft(), adView.getTop(), adView.getRight(), adView.getBottom());
                    }
                });
                Utils.emit(reactContext,"ad-loaded-"+adView.getUnitId());
            }
        });
    }
}
