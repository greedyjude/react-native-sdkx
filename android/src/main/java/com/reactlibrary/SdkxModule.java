package com.reactlibrary;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.app.Activity;

import com.facebook.react.bridge.*;
import com.greedygame.core.AppConfig;
import com.greedygame.core.GreedyGameAds;
import com.greedygame.core.adview.modals.AdRequestErrors;
import com.greedygame.core.app_open_ads.exposed.AdOrientation;
import com.greedygame.core.app_open_ads.exposed.AppOpenAdsErrors;
import com.greedygame.core.app_open_ads.exposed.AppOpenAdsEventsListener;
import com.greedygame.core.app_open_ads.exposed.GGAppOpenAds;
import com.greedygame.core.interfaces.GreedyGameAdsEventsListener;
import com.greedygame.core.interfaces.PrefetchAdsListener;
import com.greedygame.core.interstitial.general.GGAutoRefreshInterstitialAd;
import com.greedygame.core.interstitial.general.GGInterstitialAd;
import com.greedygame.core.interstitial.general.GGInterstitialEventsListener;
import com.greedygame.core.models.InitErrors;

public class SdkxModule extends ReactContextBaseJavaModule {

    private final ReactApplicationContext reactContext;

    private GGInterstitialAd mIntersitialAd;
    private GGAutoRefreshInterstitialAd mAutoRefreshingInterstitialAd;
    private Handler mainHandler;

    public SdkxModule(ReactApplicationContext reactContext) {
        super(reactContext);
        this.reactContext = reactContext;
        mainHandler = new Handler(Looper.getMainLooper());
    }

    @Override
    public String getName() {
        return "SDKX";
    }

    @ReactMethod
    public void initWith(ReadableMap appConfig, final Promise promise) {
        Context androidContext = (Context) reactContext;

        AppConfig.Builder appConfigBuilder = new AppConfig.Builder(androidContext)
                .withAppId(appConfig.getString("appId").toString())
                .enableAdmobAds(appConfig.getBoolean("enableAdmob"))
                .enableFacebookAds(appConfig.getBoolean("enableFan"))
                .enableMopubAds(appConfig.getBoolean("enableMopub"))
                .enableCrashReporting(appConfig.getBoolean("enableCrashReporting"))
                .setAdRequestTimeoutInSeconds(appConfig.getInt("adRequestTimeoutInSeconds"))
                .enableCcpa(appConfig.getBoolean("enableCcpa"))
                .enableCoppa(appConfig.getBoolean("enableCoppa"))
                .enableGdpa(appConfig.getBoolean("enableGdpr"))
                .enableDNTLocation(appConfig.getBoolean("enableDNTLocation"))
                .enableInstallTracking(appConfig.getBoolean("enableInstallTracking"))
                .enableDebug(appConfig.getBoolean("enableDebug"));


        GreedyGameAds.initWith(appConfigBuilder.build(), new GreedyGameAdsEventsListener() {
            @Override
            public void onInitSuccess() {
                promise.resolve(true);
            }

            @Override
            public void onInitFailed(InitErrors cause) {
                promise.reject(new Throwable(cause.toString()));
            }

            @Override
            public void onDestroyed() {

            }
        });
    }

    @ReactMethod
    public boolean isInitialised() {
        return GreedyGameAds.isSdkInitialized();
    }

    @ReactMethod
    public void destroy() {
        GreedyGameAds.destroy();
    }

    @ReactMethod
    public void loadInterstitialAd(String unitId, final Promise promise) {

        Context androidContext = (Context) reactContext;
        mIntersitialAd = new GGInterstitialAd(androidContext, unitId);
        mIntersitialAd.setListener(new GGInterstitialEventsListener() {
            @Override
            public void onAdLoaded() {
                Log.d("GGADS", "Ad Loaded");
                promise.resolve(true);
            }

            @Override
            public void onAdLeftApplication() {
                Log.d("GGADS", "Ad Left Application");
                Utils.emit(reactContext, "inter-ad-left-app");
            }

            @Override
            public void onAdClosed() {
                Log.d("GGADS", "Ad Closed");
                Utils.emit(reactContext, "inter-ad-closed");

            }

            @Override
            public void onAdOpened() {
                Log.d("GGADS", "Ad Opened");
                Utils.emit(reactContext, "inter-ad-opened");

            }

            @Override
            public void onAdLoadFailed(AdRequestErrors cause) {
                Log.d("GGADS", "Ad Load Failed " + cause);
                promise.reject(new Throwable(cause.toString()));
            }
        });
        mIntersitialAd.loadAd();
    }

    @ReactMethod
    public void showInterstitialAd(final Promise promise) {
        if (mIntersitialAd == null || !mIntersitialAd.isAdLoaded()) {
            promise.reject(new Throwable("Load the ad before showing it"));
            return;
        }
        mainHandler.post(new Runnable() {
            @Override
            public void run() {
                mIntersitialAd.show();
                promise.resolve(true);
            }
        });

    }

    @ReactMethod
    public void prefetchAds(ReadableArray adUnits, final Promise promise) {
        PrefetchAdsListener listener = new PrefetchAdsListener() {
            @Override
            public void onAdPrefetched(String unitId) {
                WritableMap map = Arguments.createMap();
                map.putString("unitId", unitId);
                Utils.emit(reactContext, "ad-prefetched", map);

            }

            @Override
            public void onAdPrefetchFailed(String unitId, AdRequestErrors cause) {
                WritableMap map = Arguments.createMap();
                map.putString("cause", cause.toString());
                map.putString("unitId", unitId);
                Utils.emit(reactContext, "ad-prefetch-failed", map);
            }

            @Override
            public void onPrefetchComplete() {
                promise.resolve(null);
            }
        };
        String[] adUnitArray = new String[adUnits.size()];
        for (int i = 0; i < adUnits.size(); i++) {
            adUnitArray[i] = (adUnits.getString(i));
        }
        GreedyGameAds.prefetchAds(listener, adUnitArray);
    }

    @ReactMethod
    public void loadAppOpenAds(String unitId, String orientationValue,final Promise promise) {
        Context androidContext = (Context) reactContext;
        AdOrientation orientation = AdOrientation.PORTRAIT;
        try {
             orientation = AdOrientation.valueOf(orientationValue.toUpperCase());
        } catch (Exception exception){
            Log.d("GGADS","Invalid orientation passed- "+orientationValue+". Switching to default portrait");
        }
        GGAppOpenAds.setOrientation(orientation);
        GGAppOpenAds.setListener(new AppOpenAdsEventsListener() {
            @Override
            public void onAdShowFailed(AppOpenAdsErrors cause) {
                WritableMap map = Arguments.createMap();
                map.putString("cause", cause.toString());
                Utils.emit(reactContext, "app-open-ad-show-failed", map);
            }

            @Override
            public void onAdLoaded() {
                Log.d("GGADS","App Open Ad Loaded");
                promise.resolve(true);
            }

            @Override
            public void onAdLoadFailed(AdRequestErrors cause) {
                Log.d("GGADS","App Open Ad Failed "+cause);
                promise.reject(new Throwable(cause.toString()));
            }

            @Override
            public void onAdOpened() {
                Log.d("GGADS","AppOpenAds Opened");
                Utils.emit(reactContext,"app-open-ad-opened");
            }

            @Override
            public void onAdClosed() {
                Log.d("GGADS","AppOpenAds Closed");
                Utils.emit(reactContext,"app-open-ad-closed");
            }

            @Override
            public void onAdLeftApplication() {
                Log.d("GGADS","AppOpenAds Left App");
                Utils.emit(reactContext,"app-open-ad-left-app");
            }
        });
        GGAppOpenAds.loadAd(unitId);
    }

    @ReactMethod
    public void showAppOpenAd(){
        mainHandler.post(new Runnable() {
            @Override
            public void run() {
                Activity currentActivity = reactContext.getCurrentActivity();
                GGAppOpenAds.show(currentActivity);
            }
        });
    }

    @ReactMethod
    public boolean isAppOpenAdsLoaded(){
        return GGAppOpenAds.isAdLoaded();
    }

    @ReactMethod
    public boolean isShowingAppOpenAd(){
        return GGAppOpenAds.isShowingAd();
    }

}
