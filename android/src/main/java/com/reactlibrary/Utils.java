package com.reactlibrary;

import android.content.Context;
import android.util.TypedValue;

import com.facebook.react.bridge.*;
import com.facebook.react.modules.core.DeviceEventManagerModule;

public class Utils{
    public static void emit(ReactContext reactContext,String key, WritableMap params){
        reactContext.getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
        .emit(key,params);
    }
public static void emit(ReactContext reactContext,String key){
        emit(reactContext, key,null);
    }

    public static int dp2Px(Context context,int value){
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,value,context.getResources().getDisplayMetrics());
    }
}