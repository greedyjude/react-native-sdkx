package com.reactlibrary;

import android.view.View;
import android.content.Context;
import android.widget.FrameLayout;
import android.util.Log;

import com.facebook.react.bridge.*;
import com.facebook.react.common.MapBuilder;
import com.facebook.react.uimanager.ThemedReactContext;
import com.facebook.react.uimanager.ViewGroupManager;
import com.facebook.react.uimanager.events.RCTEventEmitter;
import com.facebook.react.uimanager.annotations.ReactProp;
import com.facebook.react.uimanager.annotations.ReactPropGroup;

import com.greedygame.core.GreedyGameAds;
import com.greedygame.core.adview.GGAdview;
import com.greedygame.core.adview.interfaces.AdLoadCallback;
import com.greedygame.core.adview.modals.AdRequestErrors;
import com.greedygame.core.adview.modals.RefreshPolicy;
import com.greedygame.core.interfaces.DestoryEventListener;

public class GGAdviewWrapper extends FrameLayout{
    int lastKnownUnitWidth,lastKnownUnitHeight;
    int adsMaxHeight = -1, adsMaxWidth = -1;
    String unitId = "";
    GGAdview currentAdview;
    String uniqueEventId;
    String tag = "GG[RNGGAdviewWrapper";
    AdLoadCallback adLoadCallback = null;
    boolean isAdLoaded = false;

    public GGAdviewWrapper(final Context context){
        super(context);
        adLoadCallback = new AdLoadCallback() {
            final ReactContext reactContext = (ReactContext) context;
            boolean shouldLoadAdAgain = true;
            @Override
            public void onReadyForRefresh() {
                //Utils.emit(reactContext,"ready-for-refresh-"+uniqueEventId);
                shouldLoadAdAgain = true;
                loadAd();
                Log.d(tag, "Ad Ready for refresh");
            }

            @Override
            public void onUiiClosed() {
                Utils.emit(reactContext,"uii-closed-"+uniqueEventId);
                Log.d(tag, "Uii closed");
            }

            @Override
            public void onUiiOpened() {
                Utils.emit(reactContext,"uii-opened-"+uniqueEventId);
                Log.d(tag, "Uii Opened");
            }

            @Override
            public void onAdLoadFailed(AdRequestErrors cause) {
                Log.d(tag, "Ad Load Failed " + cause);
                WritableMap map = Arguments.createMap();
                map.putString("cause",cause.toString());
                Utils.emit(reactContext,"ad-load-failed-"+uniqueEventId,map);
            }

            @Override
            public void onAdLoaded() {
                Log.d(tag, "Ad Loaded "+uniqueEventId);
                isAdLoaded = true;
                if(currentAdview != null) {
                    rerender(currentAdview);
                    if(shouldLoadAdAgain) {
                        currentAdview.postDelayed(new Runnable(){
                            @Override
                            public void run(){
                                shouldLoadAdAgain = false;
                                currentAdview.loadAd(adLoadCallback);
                            }

                        },1000);
                    } else {
                        Utils.emit(reactContext, "ad-loaded-" + uniqueEventId);
                    }
                }
            }
        };

        GreedyGameAds.addDestroyEventListener(new DestoryEventListener(){
            @Override
            public void onDestroy(){
                Log.d(tag,"SDK-Destroyed");
                if(currentAdview != null){
                    rerender(currentAdview);
                }
            }
        });
    }

    public void setAdsMaxHeight(int adsMaxHeight) {
        Log.d(tag,"Set adsMaxHeight: "+adsMaxHeight);
        Log.d(tag,"Converted dp2px adsMaxHeight: "+Utils.dp2Px(getContext(),adsMaxHeight));
        this.adsMaxHeight = Utils.dp2Px(getContext(),adsMaxHeight);
    }

    public void setAdsMaxWidth(int adsMaxWidth) {
        Log.d(tag,"Set adsMaxHeight: "+adsMaxWidth);
        Log.d(tag,"Converted dp2px adsMaxHeight: "+Utils.dp2Px(getContext(),adsMaxWidth));

        this.adsMaxWidth = Utils.dp2Px(getContext(), adsMaxWidth);
    }

    public void setUnitId(String unitId) {
        this.unitId = unitId;
//        attachNewAdView();
//        loadAd();
    }

   public void setUniqueEventId(String id){
       this.uniqueEventId = id;
   }

    private boolean hasUnitIdSet(){
        return unitId != "";
    }

    private void attachNewAdView(){
        currentAdview= new GGAdview(getContext());
        if(adsMaxHeight >= 50){
            currentAdview.setAdsMaxHeight(adsMaxHeight);
        }
        if(adsMaxWidth >= 100){
            currentAdview.setAdsMaxWidth(adsMaxWidth);
        }

        currentAdview.setUnitId(unitId);
        currentAdview.setRefreshPolicy(RefreshPolicy.MANUAL);
        removeAllViews();
        int finalAdUnitWidth,finalAdUnitHeight= 0;
        if(adsMaxWidth != -1 && lastKnownUnitWidth > adsMaxWidth){
            finalAdUnitWidth = adsMaxWidth;
        } else {
            finalAdUnitWidth = lastKnownUnitWidth;
        }

        if(adsMaxHeight != -1 && lastKnownUnitHeight > adsMaxHeight){
            finalAdUnitHeight = adsMaxHeight;
        } else {
            finalAdUnitHeight = lastKnownUnitHeight;
        }

        FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(finalAdUnitWidth,finalAdUnitHeight);
        addView(currentAdview,lp);
    }

    void loadAd() {
        if(!hasUnitIdSet()){
            return;
        }
        Log.d(tag, "Loading ad for unitId");
        Log.d(tag,"AdSize before load"+currentAdview.getWidth()+"X"+currentAdview.getHeight());
        currentAdview.loadAd(adLoadCallback);
    }

    private void rerender(final GGAdview currentAdview){
        currentAdview.invalidate();
        currentAdview.post(new Runnable() {
            @Override
            public void run() {
                Log.d(tag,"ReDrawing the view");
                currentAdview.measure(MeasureSpec.makeMeasureSpec(currentAdview.getWidth(), MeasureSpec.EXACTLY),
                        MeasureSpec.makeMeasureSpec(currentAdview.getHeight(), MeasureSpec.EXACTLY));
                        currentAdview.layout(currentAdview.getLeft(), currentAdview.getTop(), currentAdview.getRight(), currentAdview.getBottom());
            }
        });
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeastureSpec){
        Log.d(tag, "onMeasure");
        int viewWidth = MeasureSpec.getSize(widthMeasureSpec);
        int viewHeight = MeasureSpec.getSize(heightMeastureSpec);
        if(viewWidth != 0 && viewHeight != 0 && lastKnownUnitHeight != viewHeight && lastKnownUnitWidth != viewWidth){
            lastKnownUnitWidth = viewWidth;
            lastKnownUnitHeight = viewHeight;
            Log.d(tag, "onMeasure creating new ad view");
            attachNewAdView();
            loadAd();
        }
        Log.d(tag,"Width: "+viewWidth+"X Height: "+viewHeight);
        super.onMeasure(widthMeasureSpec,heightMeastureSpec);
    }

    @Override
    public void onVisibilityAggregated(boolean isVisible ) {
        super.onVisibilityAggregated(isVisible);
        Log.d(
                tag,
                "has Lifecycle? ${mLifeCycleRef?.get() != null} Visibility Aggregated $visibility isVisible-$isVisible"
        );
        if (currentAdview != null && isAdLoaded) {
            currentAdview.loadAd(adLoadCallback);
        }
    }
}
