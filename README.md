# react-native-sdkx

## Getting started

`$ npm install react-native-sdkx --save`

### Mostly automatic installation

`$ react-native link react-native-sdkx`

## Usage
```javascript
import Sdkx from 'react-native-sdkx';
```
