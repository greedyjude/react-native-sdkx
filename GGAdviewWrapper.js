import React, { useState } from "react";
import { requireNativeComponent, DeviceEventEmitter } from "react-native";
const GGAdViewComponent = requireNativeComponent("GGAdviewWrapper");


function stringToUUID (str) 
{
  if (str === undefined || !str.length)
    str = "" + Math.random() * new Date().getTime() + Math.random();

  let c = 0,
      r = "";

  for (let i = 0; i < str.length; i++)
    c = (c + (str.charCodeAt(i) * (i + 1) - 1)) & 0xfffffffffffff;

  str = str.substr(str.length / 2) + c.toString(16) + str.substr(0, str.length / 2);
  for(let i = 0, p = c + str.length; i < 32; i++)
  {
    if (i == 8 || i == 12 || i == 16 || i == 20)
      r += "-";

    c = p = (str[(i ** i + p + 1) % str.length]).charCodeAt(0) + p + i;
    if (i == 12)
      c = (c % 5) + 1; //1-5
    else if (i == 16)
      c = (c % 4) + 8; //8-B
    else
      c %= 16; //0-F

    r += c.toString(16);
  }
  return r;
}

const GGAdviewWrapper = (props) => {
  const uniqueIdRef = React.useRef(stringToUUID());

  React.useEffect(() => {
    const adLoadFailedListener = (data) => {
      if (props.onAdLoadFailed) {
        console.log(
          "Received emitted event -ggadview",
          uniqueIdRef.current,
          "ad-load-failed",
          props.unitId
        );
        props.onAdLoadFailed(data.cause);
      }
    };
    const adLoadedListener = () => {
      if (props.onAdLoaded ) {
        console.log(
          "Received emitted event -ggadview",
          uniqueIdRef.current,
          "ad-loaded"
        );
        props.onAdLoaded();
      }
    };
    const uiiClosedListener = () => {
      if (props.onUiiClosed ) {
        console.log("Received emitted event -ggadview", "uii closed");
        props.onUiiClosed();
      }
    };
    const uiiOpenedListener = () => {
      if (props.onUiiOpened) {
        console.log(
          "Received emitted event -ggadview",
          uniqueIdRef.current,
          "uii opened"
        );
        props.onUiiOpened();
      }
    };
    const readyForRefreshListener = () => {
      if (props.onReadyForRefresh ) {
        console.log(
          "Received emitted event -ggadview",
          uniqueIdRef.current,
          "ready-for-refresh"
        );
        props.onReadyForRefresh();
      }
    };

    console.log("Adding listeners", uniqueIdRef.current);
    DeviceEventEmitter.addListener(
      "ad-load-failed-" + uniqueIdRef.current,
      adLoadFailedListener
    );
    DeviceEventEmitter.addListener(
      "ad-loaded-" + uniqueIdRef.current,
      adLoadedListener
    );
    DeviceEventEmitter.addListener(
      "uii-closed-" + uniqueIdRef.current,
      uiiClosedListener
    );
    DeviceEventEmitter.addListener(
      "uii-opened-" + uniqueIdRef.current,
      uiiOpenedListener
    );
    DeviceEventEmitter.addListener(
      "ready-for-refresh-" + uniqueIdRef.current,
      readyForRefreshListener
    );
    return () => {
      console.log("Removing listeners", uniqueIdRef);
      DeviceEventEmitter.removeListener(
        "ready-for-refresh-" + uniqueIdRef.current,
        readyForRefreshListener
      );
      DeviceEventEmitter.removeListener(
        "uii-opened-" + uniqueIdRef.current,
        uiiOpenedListener
      );
      DeviceEventEmitter.removeListener(
        "uii-closed-" + uniqueIdRef.current,
        uiiClosedListener
      );
      DeviceEventEmitter.removeListener(
        "ad-loaded-" + uniqueIdRef.current,
        adLoadedListener
      );
      DeviceEventEmitter.removeListener(
        "ad-load-failed-" + uniqueIdRef.current,
        adLoadFailedListener
      );
    };
  }, []);
  return (
      <GGAdViewComponent {...props}
      uniqueId={uniqueIdRef.current}
      />
  );
};

export default GGAdviewWrapper;
