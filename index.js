import { NativeModules, DeviceEventEmitter } from 'react-native';
export { default as GGAdview } from "./GGAdviewWrapper"
const { SDKX } = NativeModules;

export default {
  initWith: async function (params) {
    const {
      appId = "",
      enableAdmob = true,
      enableFan = false,
      enableMopub = false,
      enableCrashReporting = true,
      adRequestTimeoutInSeconds = 0,
      enableDebug = false,

      enableCcpa = false,
      enableCoppa = false,
      enableGdpr = false,
      enableDNTLocation = false,
      enableInstallTracking = true
    } = params
    console.log("Second", { SDKX })
    return await SDKX.initWith({
      appId,
      enableAdmob,
      enableFan,
      enableMopub,
      enableCrashReporting,
      adRequestTimeoutInSeconds,
      enableDebug,

      enableCcpa,
      enableCoppa,
      enableGdpr,
      enableDNTLocation,
      enableInstallTracking
    })
  },
  loadInterstitialAd: async function (unitId) {
    return await SDKX.loadInterstitialAd(unitId);
  },

  showInterstitialAd: function ({ onAdOpened, onAdLeftApplication, onAdClosed }) {
    DeviceEventEmitter.removeAllListeners("inter-ad-opened")
    DeviceEventEmitter.addListener("inter-ad-opened", (data) => {
      console.log("Received emitted event", "inter-ad-opened")
      if (onAdOpened) {
        onAdOpened()
      }
    })

    DeviceEventEmitter.removeAllListeners("inter-ad-closed")
    DeviceEventEmitter.addListener("inter-ad-closed", (data) => {
      console.log("Received emitted event", "inter-ad-closed")
      if (onAdClosed) {
        onAdClosed()
      }
    })

    DeviceEventEmitter.removeAllListeners("inter-ad-left-app")
    DeviceEventEmitter.addListener("inter-ad-left-app", (data) => {
      console.log("Received emitted event", "inter-ad-left-app")
      if (onAdLeftApplication) {
        onAdLeftApplication()
      }
    })
    return SDKX.showInterstitialAd()
  },

  prefetchAds: async function ({ adUnits, onAdPrefetched, onAdPrefetchFailed, onPrefetchComplete }) {
    DeviceEventEmitter.removeAllListeners("ad-prefetched")
    DeviceEventEmitter.addListener("ad-prefetched", (data) => {
      if (onAdPrefetched) {
        onAdPrefetched(data.unitId)
      }
    })

    DeviceEventEmitter.removeAllListeners("ad-prefetch-failed")
    DeviceEventEmitter.addListener("ad-prefetch-failed", (data) => {
      if (onAdPrefetchFailed) {
        onAdPrefetchFailed(data.unitId, data.cause)
      }
    });
    return await SDKX.prefetchAds(adUnits)

  },

  destroy: function () {
    SDKX.destroy()
  },

  isInitialized: function () {
    return SDKX.isInitialized
  },

  loadAppOpenAd: async function(unitId,orientation) {
    return await SDKX.loadAppOpenAds(unitId,orientation)
  },

  showAppOpenAd: function ({onAdShowFailed,onAdOpened,onAdClosed,onAdLeftApplication}){
    DeviceEventEmitter.removeAllListeners("app-open-ad-show-failed")
    DeviceEventEmitter.addListener("app-open-ad-show-failed",(data) => {
        console.log("Received emitted event","app-open-ad-show-failed")
        if(onAdShowFailed){
            onAdShowFailed(data.cause)
        }
    })

    DeviceEventEmitter.removeAllListeners("app-open-ad-opened")
    DeviceEventEmitter.addListener("app-open-ad-opened",()=>{
        if(onAdOpened){
            onAdOpened()
        }
    })

    DeviceEventEmitter.removeAllListeners("app-open-ad-closed")
        DeviceEventEmitter.addListener("app-open-ad-closed",()=>{
        if(onAdClosed){
            onAdClosed()
        }
    })

    DeviceEventEmitter.removeAllListeners("app-open-ad-left-app")
        DeviceEventEmitter.addListener("app-open-ad-left-app",()=>{
        if(onAdLeftApplication){
            onAdLeftApplication()
        }
    })
     SDKX.showAppOpenAd()

  }





}
